[full list containers](https://gitlab.com/mobilemediaruopensourse/iddqd/container_registry)

## How to start

1. CD to the site files folder
2. Run:
```
docker run -p 80:80 -p 5432:5432 -p 3306:3306 -v $(pwd):/server/sites/default -it --name iddqd_my_project nikitades/iddqd
```
or
```
docker run -p 80:80 -p 5432:5432 -p 3306:3306 -v $(pwd):/server/sites/default -it --name iddqd_my_project registry.gitlab.com/mobilemediaruopensource/iddqd:latest
```
That's it.

## Remember

The predefined NGINX setting is to route the request to the `/server/sites/default/public` folder.
In case if your project does not contain a public folder to split the accessible and inaccessible code parts, you shoud specify:
```
docker run -p 80:80 -p 5432:5432 -p 3306:3306 -v $(pwd):/server/sites/default/public -it  --name iddqd_my_project nikitades/iddqd 
``` 
or
```
docker run -p 80:80 -p 5432:5432 -p 3306:3306 -v $(pwd):/server/sites/default/public -it  --name iddqd_my_project registry.gitlab.com/mobilemediaruopensource/iddqd:latest
``` 
Have a look at default/**public** folder. That's where the FPM searches for the executables.

## XDebug

It's simple. **Just press "Start listening for PHP debug connections" button.**

Well, if you use OS X, that's not that simple. You have to override the default HOST_IP variable:
```
docker run -p 80:80 -p 5432:5432 -p 3306:3306 -v $(pwd):/server/sites/default -it -e HOST_IP=host.docker.internal nikitades/iddqd
```
And then just press the listening button.

## PHPStorm server dialogue

At the first xdebug start PHPStorm will show some information. Just confirm.

![Server](https://image.ibb.co/c2uzfp/server.png)

![Edit Configuration](https://image.ibb.co/iZos0p/remote_debug.png)
